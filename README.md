# DTS AndroPort Project

We bring to you a collection of ports for DTS Audio's Android techonologies to run most Android devices. While most of these are in developement, some features are working and audible differences can be noticed between having the effect on and off. We have a team of skilled Android developers from different fields working on different parts of this mod to get the whole thing working. Some of the main contributers are:

  - Laster K. [@lazerl0rd](https://gitlab.com/lazerl0rd) (Android kernel developer)
  - JohnFawkes [@JohnFawkes](https://gitlab.com/JohnFawkes) (Core member of ICEPower port team)
  - Michi [@schneidersirsch.m](https://gitlab.com/schneidersirsch.m) (Smali editor for DTS ports)
  - Josh Choo [@joshuous](https://gitlab.com/joshuous) (Android kernel and ROM developer)
  - UltraM8 [@arssovetnikov](https://gitlab.com/arssovetnikov) (Founder of Ainur Sauron)
  - And others...

## DTS Headphone:X
### Requirements
* [x] You need to request your kernel developer to add this in
* [x] You should preferebly be running a custom ROM
* [x] You need to be on at least Android Nougat
* [x] A device running a Snapdragon chip with at least WCD9330 and a Hexagon DSP

### Kernel Commits
WCD9330
```
git fetch https://gitlab.com/dts_project/kernel -t hpx-wcd9330 && git cherry-pick fbc216a072055e4eb21f6838fcb7736da7b93c45 && git cherry-pick fbc216a072055e4eb21f6838fcb7736da7b93c45..bd142dc7db91d2afccfaf68887607bbd7b17e3cc
```
WCD9335
```
git fetch https://gitlab.com/dts_project/kernel -t hpx-wcd9335 &&  git cherry-pick a8d80fb256ee6fa7fa862a775c566e1f26f39122 && git cherry-pick a8d80fb256ee6fa7fa862a775c566e1f26f39122..037d6324aed1a1b10c22eee49f8be97eb735889f
```
WCD934X
```
git fetch https://gitlab.com/dts_project/kernel -t hpx-wcd934x && git cherry-pick a953da827af9a6a2e47fbca40bb77e42ed32f613 && git cherry-pick a953da827af9a6a2e47fbca40bb77e42ed32f613..1f8b2f615399729e1a090e8b8532536249394be4
```
...and for Oreo+ (on top of the commits set above)
```
git fetch https://gitlab.com/dts_project/kernel -t && git cherry-pick 391bb4b371eab92ea0ea66ad254df0abd7cf0353
```

If you're still having issues add the following to your defconfig:
```
CONFIG_DTS_SRS_TM=y
```
## DTS:X Ultra
### Requirements
* [x] You need to request your kernel developer to add this in
* [x] You should be on Oreo or higher

### Kernel Commits
Universal
```
git fetch https://gitlab.com/dts_project/kernel -t ultra && git cherry-pick c5b95ad1248761909a9d1891898b3cd35c377701 && git cherry-pick c5b95ad1248761909a9d1891898b3cd35c377701..229e94198375698866007e1dca6aff1821a80386
```
## Post-kernel implementation
Don't forget to tell your users to collect the userland side of the port (choose one) from https://forum.xda-developers.com/android/software/port-dtsx-ultra-dts-headphonex-t3896233!

## Contact Us
Join our Telegram Group at https://t.me/DTSWizardproxy/.